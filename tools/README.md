# Pepper and Carrot MINI tools

All tools used in the derivative webcomic [Pepper and Carrot MINI](https://www.peppercarrot.com/?static11/community-webcomics&page=Pepper-and-Carrot-Mini_by_Nartance/)

## You can find here:
- `fonts`: a folder to explain how to use a new font;
- `svg`: all external `.svg` files used during the conception of an episode (speech bubbles, effects, etc.), with the author, the link to main file, and the license.
