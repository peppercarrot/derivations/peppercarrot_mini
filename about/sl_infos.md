Humorističen spletni strip, v katerem "miceni" Paprika, Korenček in ostali prebivalci Hereve uganjajo zabavne vragolije.
<br>Prevajanje stripa je mogoče prek [repozitorija na Framagitu](https://framagit.org/peppercarrot/derivations/peppercarrot_mini).
Več o avtorju piše na [njegovem profilu na deviantArtu](https://nartance.deviantart.com/).
<br>Prevedel: [Gorazd Gorup](https://framagit.org/Grimpy101) | Preveril in uredil: [Andrej Ficko](https://framagit.org/fici)
