Ein humorvoller Webcomic, in dem Pepper, Carrot und ihre Freunde alle "miniaturisiert" vorkommen.
Die Übersetzungsanleitungen befinden sich auf [dem Framagit Repositorium](https://framagit.org/peppercarrot/derivations/peppercarrot_mini).
Weitere Informationen über den Autor finden Sie [auf seinem deviantArt-Profil](https://nartance.deviantart.com/).
<br>Deutsche Übersetzung: [Andrej Ficko](https://framagit.org/fici) (kein Muttersprachler) | Korrektur: [Ret Samys](https://framagit.org/RetSamys)
