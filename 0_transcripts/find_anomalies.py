#!/usr/bin/env python3
# encoding: utf-8
#
# find_anomalies.py
#
#  SPDX-License-Identifier: GPL-3.0-or-later
#  SPDX-FileCopyrightText: © 2023 Andrej Ficko

import os
from pathlib import Path
import sys

"""
This script looks for anomalies of 4 types: (you can use lang code as argument)
1. Compares episode filenames for each language with the French ones.
   - .gitkeep happens when you create a folder using the GitLab GUI (to preserve empty folder) and can be deleted.
   - readme is an agreed name for notes, .txt or .md.

2. Checks the xlink:href of every .svg file to match the .svg filename and if exists in lang folder.
"""

# from https://svn.blender.org/svnroot/bf-blender/trunk/blender/build_files/scons/tools/bcolors.py
class bcolors:
    if os.name == 'posix': # only works on Linux
        HEADER = '\033[95m'
        OKBLUE = '\033[94m'
        OKCYAN = '\033[96m'
        OKGREEN = '\033[92m'
        WARNING = '\033[93m'
        FAIL = '\033[91m'
        ENDC = '\033[0m'
        BOLD = '\033[1m'
        UNDERLINE = '\033[4m'
    else:
        HEADER, OKBLUE, OKCYAN, OKGREEN, WARNING, FAIL, ENDC, BOLD, UNDERLINE = '','','','','','','','',''

scriptpath = os.path.abspath(__file__)
directory = Path(os.path.dirname(scriptpath)).parent

print ("\n" + bcolors.BOLD + "Searching for anomalies in the folder structure:" + bcolors.ENDC)
langdir = directory / 'lang'
frdirlist=os.listdir(langdir / 'fr')
dirlist = os.listdir(langdir)
for lang in dirlist:
    for l_file in os.listdir(langdir / lang):
        if l_file not in frdirlist:
            notify_color=bcolors.WARNING
            if l_file == ".gitkeep":
                notify_color=bcolors.OKCYAN
            elif "readme." in l_file.lower():
                notify_color=bcolors.OKGREEN
            print("/lang/"+lang+"/"+ notify_color + l_file + bcolors.ENDC)

print ("\n" + bcolors.BOLD + "Searching for anomalies in SVG artwork xlink:href:" + bcolors.ENDC + " (takes some time)")
langdir = directory / 'lang'
dirlist = os.listdir(langdir)
for lang in dirlist:
    for l_file in os.listdir(langdir / lang):
        if l_file.endswith(".svg"):
            with open(langdir / lang / l_file, 'r', encoding="utf-8") as svg_file:
                epnum = l_file.strip(".svg").strip("PCMINI_")
                svg_data = svg_file.read()
                svg_href_loc = svg_data.find('xlink:href=".')
                href_link = "i"+ svg_data[svg_href_loc: svg_data.find('"\n',svg_href_loc)].strip('xlink:href="../../')
                if href_link == "" or epnum not in href_link or not os.path.exists(directory / (href_link)) :
                    print (lang, l_file, href_link)
