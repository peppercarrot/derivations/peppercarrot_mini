# Pepper and Carrot MINI repo

![cover image](./img/_cover/pepper_mini_vs_real_pepper_thumb.png)

This repository contains all the files necessary to translate the spinoff webcomic [Pepper and Carrot MINI](
https://peppercarrot.com/xx/fan-art/fan-comics__Pepper-and-Carrot-Mini_by_Nartance.html), based on [Pepper&Carrot](http://www.peppercarrot.com) by David Revoy.

## Main repo

You can find the repo of the main comic [here](https://framagit.org/peppercarrot). I'm trying to copy the way of translating to simplify the work of translators. The veterans won't be lost, and the new ones can find reliable documentation on the main repo. 

## How to translate

As said above, it's the same system as the main repo. You can find documentation [here](https://framagit.org/peppercarrot/webcomics/blob/master/CONTRIBUTING.md), to see how it is done for the main comic. There are some changes in the folders here, explained in the [CONTRIBUTING.md](CONTRIBUTING.md) file of this repo.

In a nutshell: you need to install Inkscape and fonts (see the subfolder `tools/fonts` for more details), clone the repo, and copy/paste a folder in the `lang/` repo, give the [ISO name](https://framagit.org/peppercarrot/webcomics/-/blob/master/langs.json) of your language, and edit all the files.

You can also add the little description for the website in the `about/` folder.

## References

Pepper and Carrot MINI is based on Pepper&Carrot. You can see the translation file for all elements of the main universe [here](https://framagit.org/peppercarrot/webcomics/-/tree/master/1_translation-names-references). If you are curious, you can also visit the [wiki](https://peppercarrot.com/xx/wiki/index.html) of the main website to learn more about the Pepper&Carrot universe... and maybe contribute ? :o)

## Credits

Read [AUTHORS.md](AUTHORS.md) in the root of this repository for the full attribution.

## Sources artworks and rendering

The artwork in the `img` subfolders are the base of the translation. Do not edit or propose a commit for the artworks. 
All the renders are in this [specific project](https://framagit.org/Nartance/peppercarrotmini_export) in the `export` folder, before displayed on the main website. If you want your translation to appear, just ask me by [mail](mailto:nicolas.artance@gmail.com) for a render :o) (anyway, renders are done if I see changes on the svg files).


## Fonts

You can find the infos about fonts used in Pepper and Carrot MINI into the subfolder `tools/fonts` to learn more about the fonts, and their licenses.

## License

Content in this repository is licensed under the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) License, except for the fonts in the `tool/fonts/` directory, which are released under their own separate license agreements.

By submitting content to this repository, one agrees to the [contributor's terms](CONTRIBUTING.md).
